#ifndef LIFE_H_
#define LIFE_H_

#include <nox/logic/actor/Component.h>
//#include <glm/vec3.hpp>
//#include <nox/logic/actor/Actor.h>
#include <nox/logic/physics/box2d/BulletSimulation3d.h>
class Life : public nox::logic::actor::Component
{
public:
	static const IdType NAME;
	bool initialize(const Json::Value& componentJsonObject) override;
	const IdType& getName() const override;

	void serialize(Json::Value& componentObject) override;
	void onCollision(nox::logic::physics::BulletSimulation3d::CollisionParameters f);
	int health;

private:
	
	

};









#endif