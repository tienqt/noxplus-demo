
#include "TimeTest.h"



namespace nox{ namespace logic
{

const TimeTest::IdType TimeTest::NAME = "TimeTest";
TimeTest::~TimeTest() = default;

const TimeTest::IdType& TimeTest::getName() const
{
	return NAME;
}

bool TimeTest::initialize(const Json::Value& componentJsonObject)
{
	if (this->TimeLoggingComponent::initialize(componentJsonObject) == false)
	{
		return false;
	}

	this->counter2 = 2;
	this->counter = 0;
	return true;
}

void TimeTest::serialize(Json::Value& componentObject)
{

}

void TimeTest::onUpdate(const Duration& deltaTime)
{
	//std::printf("%i\n", counter);
	this->logComponentData();
}

void TimeTest::onCreate()
{
	TimeLoggingComponent::onCreate();
}

void TimeTest::onDestroy()
{

}

void TimeTest::onDeactivate()
{

}

void TimeTest::onActivate()
{

}

void TimeTest::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
}

void TimeTest::onSave(world::timemanipulation::TimeManipulationData3d* saveData)
{
	//saveData->save<int>(&counter2);
	saveData->save<int>(&counter);
	//saveData->save<BUMB>(&testStruct);
	testStruct.en = 2;
	testStruct.tre = 1;
	testStruct.fire = "RAMP!";
	counter2 += 200;
	this->counter++;
	
}

void TimeTest::onRestore(world::timemanipulation::TimeManipulationData3d* restoreData)
{
	restoreData->load<int>(&counter);
	//restoreData->load<int>(&counter2);
	//restoreData->load<BUMB>(&testStruct);
	int i = 0;

}

}}
