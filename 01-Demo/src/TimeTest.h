#ifndef TIME_TEST_H_
#define TIME_TEST_H_

#include <nox/logic/world/timeManipulation/TimeLoggingComponent.h>
#include <string>

namespace nox { namespace logic 
{

	struct BUMB
	{
		int en = 1;
		int to = 2;
		int tre = 3;
		std::string fire = "fire";
	};

class TimeTest : public world::timemanipulation::TimeLoggingComponent
{
public:
	static const IdType NAME;

	~TimeTest();

	const IdType& getName() const override;

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onUpdate(const Duration& deltaTime) override;
	void onCreate() override;
	void onDestroy() override;
	void onDeactivate() override;
	void onActivate() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;

	void onSave(world::timemanipulation::TimeManipulationData3d* saveData) override;
	void onRestore(world::timemanipulation::TimeManipulationData3d* restoreData) override;

private:

	int counter;
	int counter2;
	BUMB testStruct;
	

};

}}









#endif