/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "TrdRenderer.h"

#include <nox/app/IContext.h>
#include <nox/app/graphics/IRenderer3d.h>
#include <nox/logic/IContext.h>
#include <nox/logic/physics/actor/ActorPhysics3d.h>
#include <nox/logic/graphics/actor/ActorGraphics3d.h>
#include <nox/logic/graphics/event/ActorGraphicsCreated3d.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/actor/component/Transform3d.h>
#include <nox/logic/actor/event/ActorCollision3d.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/event/TransformChange3d.h>
#include <nox/logic/control/actor/ActorDirectionControl3d.h>
#include <nox/logic/world/Manager.h>

#include <glm/gtx/transform.hpp>	// For glm::translate(..)


TrdRenderer::TrdRenderer(nox::app::IContext* applicationContext, const std::string& windowTitle) :
nox::window::RenderSdlWindowView(applicationContext, windowTitle, false, true),
eventBroadcaster(nullptr),
renderer(nullptr),
camera(std::make_shared<nox::app::graphics::Camera3d>(this->getWindowSize())),
listener("TrdRenderer"),
mouseJointId(-1),
leftMousePressed(false),
leftMouseActor(nullptr),
lastDirection(glm::vec3())
{
	this->log = applicationContext->createLogger();
	this->log.setName("TrdRenderer");

	this->listener.addEventTypeToListenFor(nox::logic::graphics::SceneNodeEdited::ID);
	this->listener.addEventTypeToListenFor(nox::logic::actor::TransformChange3d::ID);
	this->listener.addEventTypeToListenFor(nox::logic::actor::ActorGraphicsCreated3d::ID);
	this->listener.addEventTypeToListenFor(nox::logic::graphics::SceneNodeEdited::ID);
	this->listener.addEventTypeToListenFor(nox::logic::world::ActorCreated::ID);
	this->listener.addEventTypeToListenFor(nox::logic::actor::ActorCollision3d::ID);
	this->rootSceneNode = std::make_shared<nox::app::graphics::TransformationNode3d>();


	graphicsAssetManager = std::make_shared<nox::app::graphics::GraphicsAssetManager3d>();
}

bool TrdRenderer::initialize(nox::logic::IContext* context)
{
	if (this->RenderSdlWindowView::initialize(context) == false)
	{
		return false;
	}

	this->listener.setup(this, context->getEventBroadcaster(), nox::logic::event::ListenerManager::StartListening_t());

	this->eventBroadcaster = context->getEventBroadcaster();

	auto resourceAccess = this->getApplicationContext()->getResourceAccess();

	auto controlLayoutResourceDescriptor = nox::app::resource::Descriptor{ "controls.json" };
	auto controlLayoutResource = resourceAccess->getHandle(controlLayoutResourceDescriptor);

	if (controlLayoutResource == nullptr)
	{
		this->log.error().format("Failed loading control layout resource \"%s\".", controlLayoutResourceDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		this->controlMapper.loadKeyboardLayout(controlLayoutResource);
	}

	return true;
}

void TrdRenderer::onRendererCreated3d(nox::app::graphics::IRenderer3d* renderer)
{
	assert(renderer != nullptr);

	renderer->setGraphicsAssetManager(graphicsAssetManager);

	this->renderer = renderer;

	this->renderer->setCamera(camera);

	this->renderer->setRootSceneNode(rootSceneNode);
}

void TrdRenderer::onRendererCreated(nox::app::graphics::IRenderer* renderer)
{

}

/*
* This is called from nox::logic::View when it has changed its controlled Actor (from e.g. loading the world).
*/
void TrdRenderer::onControlledActorChanged(nox::logic::actor::Actor* controlledActor)
{
	/*
	* We make shure that our controlMapper knows which Actor it is controlling so that the events are
	* sent correctly.
	*/
	this->controlMapper.setControlledActor(controlledActor);
	auto controlComponent = this->getControlledActor()->findComponent<nox::logic::control::ActorDirectionControl3d>();
	if (controlComponent != nullptr)
	{
		// Fix getter and setter or move somewhere else?
		if (controlComponent->camera == nullptr)
		{
			controlComponent->camera = camera.get();
		}
	}
}

void TrdRenderer::onWindowSizeChanged(const glm::uvec2& size)
{
	this->RenderSdlWindowView::onWindowSizeChanged(size);

	this->camera->setSize(size);
}

std::shared_ptr<nox::app::graphics::Camera3d> TrdRenderer::getCamera()
{
	return this->camera;
}

void TrdRenderer::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{


	if (event->isType(nox::logic::graphics::SceneNodeEdited::ID))
	{
		auto sceneNodeEditied = static_cast<nox::logic::graphics::SceneNodeEdited*>(event.get());
		if (sceneNodeEditied->getEditAction() == nox::logic::graphics::SceneNodeEdited::Action::CREATE)
		{
			if (rootSceneNode->hasNodeInTree(sceneNodeEditied->getSceneNode()) == false)
				rootSceneNode->addChild(sceneNodeEditied->getSceneNode());
		}
		else if (sceneNodeEditied->getEditAction() == nox::logic::graphics::SceneNodeEdited::Action::REMOVE)
		{
			if (rootSceneNode->hasNodeInTree(sceneNodeEditied->getSceneNode()))
			rootSceneNode->removeChild(sceneNodeEditied->getSceneNode());
		}
		else if (sceneNodeEditied->getEditAction() == nox::logic::graphics::SceneNodeEdited::Action::TIME_ADD)
		{
			if (rootSceneNode->hasNodeInTree(sceneNodeEditied->getSceneNode()) == false)
				rootSceneNode->addChild(sceneNodeEditied->getSceneNode());
		}
		else if (sceneNodeEditied->getEditAction() == nox::logic::graphics::SceneNodeEdited::Action::TIME_REMOVE)
		{
			if (rootSceneNode->hasNodeInTree(sceneNodeEditied->getSceneNode()))
			rootSceneNode->removeChild(sceneNodeEditied->getSceneNode());
		}
		
	}
	else if (event->isType(nox::logic::actor::ActorGraphicsCreated3d::ID))
	{
	//	const auto actorGraphics = static_cast<nox::logic::actor::ActorGraphicsCreated3d*>(event.get());
	//	this->graphicsAssetManager->loadAssetFromFile(actorGraphics->getPath(), actorGraphics->getName());	
	} 
	else if (event->isType(nox::logic::actor::ActorCollision3d::ID))
	{
		const auto test = static_cast<nox::logic::actor::ActorCollision3d*>(event.get());

		if (test->isStartingCollision() == true)
		{
			//std::cout << "collision event started \n";
		}
		else if (test->isStartingCollision() == false)
		{
			//std::cout << "collision event stopped \n";
		}
		
	}
}

void TrdRenderer::onMousePress(const SDL_MouseButtonEvent& event)
{
	if (event.button == SDL_BUTTON_RIGHT)
	{
		//leftMouseActor = this->getLogicContext()->getPhysics3d()->findActorRayIntersectingPoint(glm::vec2(event.x, event.y), 1000.0f, glm::vec2(this->getWindowSize().x, this->getWindowSize().y), camera->getViewProjectionMatrix());
		//if (leftMouseActor != nullptr)
		//{
		//	leftMousePressed = true;
		//	//std::printf("Actor: %s is beign controlled\n", leftMouseActor->getName().c_str());
		//	auto controlledActorPhysics = getControlledActor()->findComponent<nox::logic::physics::ActorPhysics3d>();
		//	controlledActorPhysics->setLinearVelocity(glm::vec3(0.0, 0.0, 0.0));
		//	setControlledActor(leftMouseActor);
		//}
	}
}


glm::vec3 TrdRenderer::convertMouseToWorld(float mouseX, float mouseY)
{

	glm::vec3 normalizedDeviceCoords = glm::vec3();

	normalizedDeviceCoords.x = static_cast<float>((2.0f * mouseX) / this->getWindowSize().x -1);
	normalizedDeviceCoords.y = static_cast<float>((2.0f * mouseY) / this->getWindowSize().y);

	glm::vec4 clipCoords = glm::vec4(normalizedDeviceCoords.x, normalizedDeviceCoords.y, normalizedDeviceCoords.z, 1);
	glm::vec4 eyeCoords = (glm::inverse(this->camera->getProjectionMatrix())) * clipCoords;
	glm::vec4 worldCoords = (glm::inverse(this->camera->getViewMatrix())) * eyeCoords;

	return glm::vec3(worldCoords.x, worldCoords.y, worldCoords.z);
	
}

void TrdRenderer::onMouseRelease(const SDL_MouseButtonEvent& event)
{
}

void TrdRenderer::onMouseMove(const SDL_MouseMotionEvent& event)
{
}

void TrdRenderer::onMouseScroll(const SDL_MouseWheelEvent& event)
{

}

void TrdRenderer::onKeyPress(const SDL_KeyboardEvent& event)
{
	if (this->controlMapper.hasControlledActor() == true)
	{
		auto controlEvents = this->controlMapper.mapKeyPress(event.keysym);

		if (this->eventBroadcaster != nullptr)
		{
			for (const auto& event : controlEvents)
			{
				this->eventBroadcaster->queueEvent(event);
			}
		}
	}
}

void TrdRenderer::onKeyRelease(const SDL_KeyboardEvent& event)
{
	if (this->controlMapper.hasControlledActor() == true)
	{
		auto controlEvents = this->controlMapper.mapKeyRelease(event.keysym);

		if (this->eventBroadcaster != nullptr)
		{
			for (const auto& event : controlEvents)
			{
				this->eventBroadcaster->queueEvent(event);
			}
		}
	}
}

nox::app::graphics::IRenderer3d* TrdRenderer::getIRenderer()
{
	return this->renderer;
}

nox::app::graphics::GraphicsAssetManager3d* TrdRenderer::getAssetManager()
{
	return this->graphicsAssetManager.get();
}

