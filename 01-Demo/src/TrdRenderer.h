/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#ifndef TRDRENDERER_H_
#define TRDRENDERER_H_

#include <nox/window/RenderSdlWindowView.h>
#include <nox/window/SdlKeyboardControlMapper.h>

#include <nox/app/graphics/Camera3d.h>
#include <nox/app/graphics/TransformationNode3d.h>
#include <nox/app/graphics/GraphicsAssetManager3d.h>

#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/actor/Actor.h>


class TrdRenderer final : public nox::window::RenderSdlWindowView
{
public:
	TrdRenderer(nox::app::IContext* applicationContext, const std::string& windowTitle);
	std::shared_ptr<nox::app::graphics::Camera3d> getCamera();
	nox::app::graphics::IRenderer3d* getIRenderer();
	nox::app::graphics::GraphicsAssetManager3d* getAssetManager();
	glm::vec3 convertMouseToWorld(float mouseX, float mouseY);

private:
	void onRendererCreated(nox::app::graphics::IRenderer* renderer) override;
	void onRendererCreated3d(nox::app::graphics::IRenderer3d* renderer) override;
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	bool initialize(nox::logic::IContext* context) override;

	void onWindowSizeChanged(const glm::uvec2& size) override;
	void onMousePress(const SDL_MouseButtonEvent& event) override;
	void onMouseRelease(const SDL_MouseButtonEvent& event) override;
	void onMouseMove(const SDL_MouseMotionEvent& event) override;
	void onMouseScroll(const SDL_MouseWheelEvent& event) override;
	void onKeyPress(const SDL_KeyboardEvent& event) override;
	void onKeyRelease(const SDL_KeyboardEvent& event) override;
	void onControlledActorChanged(nox::logic::actor::Actor* controlledActor) override;

	bool leftMousePressed;
	nox::logic::actor::Actor* leftMouseActor;
	glm::vec3 lastDirection;

	
	
	nox::app::log::Logger log;
	nox::logic::event::ListenerManager listener;
	nox::window::SdlKeyboardControlMapper controlMapper;

	nox::logic::event::IBroadcaster* eventBroadcaster;

	nox::app::graphics::IRenderer3d* renderer;
	std::shared_ptr<nox::app::graphics::Camera3d> camera;
	std::shared_ptr<nox::app::graphics::TransformationNode3d> rootSceneNode;

	int mouseJointId;
	
	std::shared_ptr<nox::app::graphics::GraphicsAssetManager3d> graphicsAssetManager;

};

#endif
