/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#include "TrdApplication.h"
#include "Life.h"

#include "TimeTest.h"

#include <nox/logic/graphics/event/DebugRenderingEnabled.h>
#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/loader/OggLoader.h>
#include <nox/app/resource/loader/JsonLoader.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/app/audio/openal/OpenALSystem.h>
#include <nox/app/storage/DataStorageBoost.h>
#include <nox/logic/event/Manager.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/world/Loader.h>
#include <nox/logic/graphics/actor/ActorGraphics3d.h>
#include <nox/logic/physics/actor/ActorPhysics3d.h>
#include <nox/logic/physics/box2d/BulletSimulation3d.h>
#include <nox/logic/control/actor/ActorDirectionControl3d.h>
#include <nox/logic/control/actor/ActorRotationControl3d.h>
#include <nox/logic/graphics/actor/ActorLight3d.h>
#include <nox/logic/world/timeManipulation/ITimeManager3d.h>
#include <nox/logic/world/timeManipulation/TimeManipulationManager3d.h>
#include <nox/logic/world/timeManipulation/DefaultIWolrdLogger3d.h>
#include <nox/logic/world/timeManipulation/DefaultITimeConflictSolver3d.h>
#include <nox/logic/world/timeManipulation/TimeManipulationComponent.h>
#include <nox/app/graphics/BaseLight3d.h>
#include <BulletCollision/Gimpact/btGImpactShape.h>
#include <glm/vec3.hpp>

//#include <glm/gtc/quaternion.hpp>

namespace event
{
	const nox::logic::event::Event::IdType TPS_UPDATE = "game.tps_update";
}

TrdApplication::TrdApplication() :
SdlApplication("3D test application", "Hookon - Even - tien"),
listener(this->getName()),
logic(nullptr),
world(nullptr),
window(nullptr),
rewind(false)
{

	//assetsmporter = std::make_shared<Assimp::Importer>();
	for (int i = 0; i < NUM_OF_KEYS; i++)
	{
		buttons.push_back(0);
	}

}

bool TrdApplication::initializeResourceCache()
{
	const auto cacheSize = 512u;
	auto cache = std::make_unique<nox::app::resource::LruCache>(cacheSize);

	cache->setLogger(this->createLogger());

	this->logger.verbose().format("LRU Cache initialized with a size of %uMB", cacheSize);

	const auto engineAssetsDir = std::string{ "../noxpluss-engine/assets" };
	const auto gameAssetsDir = std::string{ "assets" };
	const auto exampleAssetsDir = std::string{ "assets" };

	if (cache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(engineAssetsDir)) == false)
	{
		this->logger.error().format("Could not create resource provider for engine assets: %s", engineAssetsDir.c_str());
		return false;
	}
	else
	{
		this->logger.verbose().format("Initialized filesystem resource provider for directory \"%s\"", engineAssetsDir.c_str());
	}

	if (cache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(gameAssetsDir)) == false)
	{
		this->logger.error().format("Could not create resource provider for game assets: %s", gameAssetsDir.c_str());
		return false;
	}
	else
	{
		this->logger.verbose().format("Initialized filesystem resource provider for directory \"%s\"", gameAssetsDir.c_str());
	}

	if (cache->addProvider(std::make_unique<nox::app::resource::BoostFilesystemProvider>(exampleAssetsDir)) == false)
	{
		this->logger.error().format("Could not create resource provider for example assets: %s", exampleAssetsDir.c_str());
		return false;
	}
	else
	{
		this->logger.verbose().format("Initialized filesystem resource provider for directory \"%s\"", exampleAssetsDir.c_str());
	}

	cache->addLoader(std::make_unique<nox::app::resource::OggLoader>());
	this->logger.verbose().raw("Initialized ogg resource loader.");

	cache->addLoader(std::make_unique<nox::app::resource::JsonLoader>(this->createLogger()));
	this->logger.verbose().raw("Initialized JSON resource loader.");

	this->setResourceCache(std::move(cache));

	

	return true;
}


bool TrdApplication::initializeDataStorage()
{
	auto dataStorage = std::make_unique<nox::app::storage::DataStorageBoost>();
	const std::string storageDir = this->getStorageDirectoryPath(false);

	if (storageDir.empty() == true)
	{
		this->logger.error().raw("Could not find a suitable storage directory.");
		return false;
	}
	else
	{
		try
		{
			dataStorage->initialize(storageDir);

			this->logger.verbose().format("Data storage initialized to \"%s\" directory.", storageDir.c_str());
			this->setDataStorage(std::move(dataStorage));
		}
		catch (nox::app::storage::DataStorageBoost::IntializationException& exception)
		{
			this->logger.error().format("Could not initialize data storage directory \"%s\": %s", storageDir.c_str(), exception.what());
			return false;
		}
	}

	return true;
}

bool TrdApplication::initializeAudio()
{
	auto system = std::make_unique<nox::app::audio::OpenALSystem>();

	if (system->initialize() == false)
	{
		return false;
	}

	this->logger.verbose().raw("Initialized OpenAL audio system.");

	this->setAudioSystem(std::move(system));

	return true;
}

bool TrdApplication::initializeLogic()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	this->logic = logic.get();

	this->addProcess(std::move(logic));

	auto physics = std::make_unique<nox::logic::physics::BulletSimulation3d>(this->logic);
	physics->setLogger(this->createLogger());
	this->logic->setPhysics(std::move(physics));
	
	auto worldHandler = std::make_unique<nox::logic::world::Manager>(this->logic);
	this->world = worldHandler.get();
	
	this->logic->setWorldManager(std::move(worldHandler));

	auto conflictSolver = std::make_unique<nox::logic::world::timemanipulation::DefaultTimeConflictSolver3d>();
	auto worldLogger = std::make_unique<nox::logic::world::timemanipulation::DefaultWorldLogger3d>(this->logic, conflictSolver.get());
	auto timeManager = std::make_unique<nox::logic::world::timemanipulation::TimeManipulationManager3d>(std::move(worldLogger), std::move(conflictSolver));
	this->logic->getPhysics3d()->setTimeManager(timeManager.get());
	this->logic->setTimeManager(std::move(timeManager));
	
	
	//std::function<void(nox::logic::physics::BulletSimulation3d::CollisionParameters)> funcStop2 = [&](nox::logic::physics::BulletSimulation3d::CollisionParameters param) {};

	//this->logic->getTimeManager()->setPause(true);
	this->logic->pause(true);

	// TODO: load 3d components
	this->world->registerActorComponent<nox::logic::actor::Transform3d>();
	this->world->registerActorComponent<nox::logic::graphics::ActorGraphics3d>();
	this->world->registerActorComponent<nox::logic::physics::ActorPhysics3d>();
	this->world->registerActorComponent<nox::logic::control::ActorDirectionControl3d>();
	this->world->registerActorComponent<nox::logic::control::ActorRotationControl3d>();
	this->world->registerActorComponent<nox::logic::graphics::ActorLight3d>();
	//this->world->registerActorComponent<nox::logic::world::timemanipulation::TimeManipulationComponent>();
	//this->world->registerActorComponent<nox::logic::TimeTest>();
	//Test for registering an actor component for Collision Detection:	
	this->world->registerActorComponent<Life>();

	auto actorDir = std::string{ "actor" };
	this->logger.verbose().format("Loading actor definitions from resource directory \"%s\"", actorDir.c_str());
	this->world->loadActorDefinitions(this->getResourceAccess(), actorDir);


	this->logger.verbose().raw("Initialized logic");

	return true;
}

bool TrdApplication::initializeWindow()
{
	lowestFps = 100;

	this->logger.verbose().raw("Initializing SDL video subsystem.");

	if (this->initializeSdlSubsystem(SDL_INIT_VIDEO) == false)
	{
		return false;
	}

	auto window = std::make_unique<TrdRenderer>(this, this->getName());
	this->window = window.get();
	this->logic->addView(std::move(window));

	this->logic->getPhysics3d()->setGraphicsAssetManager(this->window->getAssetManager());

	//this->logic->getTimeManager()->getConflictSolver()->setOnPlaybackStarted([&](nox::logic::world::Manager* manager) {
	//	auto newActor = manager->createActorFromDefinitionName("capsule");

	//	if (newActor != nullptr)
	//	{
	//		newActor->create();
	//		newActor->activate();
	//		auto newActorFUUU = newActor.get();
	//		manager->manageActor(std::move(newActor));

	//		auto actorPhysics = newActorFUUU->findComponent<nox::logic::physics::ActorPhysics3d>();
	//		auto actorTransform = newActorFUUU->findComponent<nox::logic::actor::Transform3d>();
	//		//	actorTransform->setPosition(glm::vec3(0, 20, 0));
	//		//actorPhysics->setTransform(glm::vec3(0, 20, 0), actorTransform->getQuaternion(), actorTransform->getScale());

	//		actorPhysics->setPosition(glm::vec3(0, 20, 0));
	//		printf("actor created! Position %f %f %f\n", actorTransform->getPosition().x, actorTransform->getPosition().y, actorTransform->getPosition().z);
	//	}

	//	printf("Playback started\n");
	//});

	auto debugRenderer = this->window->getIRenderer()->getDebugRenderer();

	static_cast<nox::logic::physics::BulletSimulation3d*>(this->logic->getPhysics3d())->setDebugRenderer(debugRenderer);

	this->logger.verbose().raw("Initialized TrdRenderer.");

	return true;
}

bool TrdApplication::loadWorld()
{
	// TODO: Create 3d actors in world json
	const auto worldFileDescriptor = nox::app::resource::Descriptor{ "world/sample.json" };
	const auto worldFileHandle = this->getResourceAccess()->getHandle(worldFileDescriptor);

	if (worldFileHandle == nullptr)
	{
		this->logger.error().format("Could not load world: %s", worldFileDescriptor.getPath().c_str());
		return false;
	}
	else
	{
		const auto jsonData = worldFileHandle->getExtraData<nox::app::resource::JsonExtraData>();

		if (jsonData == nullptr)
		{
			this->logger.error().format("Could not get JSON data for world: %s", worldFileDescriptor.getPath().c_str());
			return false;
		}
		else
		{
			auto loader = nox::logic::world::Loader{ this->logic };
			loader.registerControllingView(0, this->window);

			//TODO: load and parse 3d actors in loader.loadWorld(...)
			if (loader.loadWorld(jsonData->getRootValue(), this->world) == false)
			{
				this->logger.error().format("Failed loading world \"%s\".", worldFileDescriptor.getPath().c_str());
				return false;
			}
		}
	}

	this->logger.verbose().format("Loaded world \"%s\"", worldFileDescriptor.getPath().c_str());



	return true;
}


bool TrdApplication::onInit()
{
	this->SdlApplication::onInit();
	angle = 0;
	this->logger = this->createLogger();
	this->logger.setName("TrdApplication");

	this->setTpsUpdateInterval(std::chrono::milliseconds(500));
	this->tpsUpdateTimer.setTimerLength(std::chrono::milliseconds(500));


	if (this->initializeResourceCache() == false)
	{
		return false;
	}

	if (this->initializeDataStorage() == false)
	{
		return false;
	}

	if (this->initializeAudio() == false)
	{
		return false;
	}

	if (this->initializeLogic() == false)
	{
		return false;
	}

	if (this->initializeWindow() == false)
	{
		return false;
	}

	if (this->loadWorld() == false)
	{
		return false;
	}

	this->listener.setup(this, this->logic->getEventBroadcaster(), nox::logic::event::ListenerManager::StartListening_t());
	this->listener.addEventTypeToListenFor(event::TPS_UPDATE);

	auto phys = dynamic_cast<nox::logic::physics::BulletSimulation3d*>(this->logic->getPhysics3d());


	//this->logic->getTimeManager()->getConflictSolver()->setOnReplayFinished([](nox::logic::world::Manager* worldManager){

	//	auto allActors = worldManager->getAllActors();
	//	for (int i = 3; i < allActors.size(); i++)
	//	{
	//		auto actorGraphics = allActors.at(i)->findComponent<nox::logic::graphics::ActorGraphics3d>();
	//		actorGraphics->overLayColor = glm::vec3(0.0f, 0.5f, 0.0f);
	//	}

	//});


//	phys->removeCallbackForActor(id, nox::logic::physics::BulletSimulation3d::CALLBACKTYPE::STOPCALLBACK, collisionNumber);
	//this->logic->pause(true);


	//// PERFORMANCE TESTING /////////////////////////////////////////////////////////


	

	//this->logic->getTimeManager()->getConflictSolver()->setConflictSolverFunction(, nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED, [](nox::logic::world::timemanipulation::ConflictParameters param){
	//	auto actorGraphicsOne = param.conflictingActor->findComponent<nox::logic::graphics::ActorGraphics3d>();
	//	actorGraphicsOne->overLayColor = glm::vec3(0.0, 0.5, 0.0);

	//	auto actorPhysicsOne = param.conflictingActor->findComponent<nox::logic::physics::ActorPhysics3d>();
	//	actorPhysicsOne->setSimulatedFlag(false);

	//	auto actorGraphicsTwo = param.affectedActor->findComponent<nox::logic::graphics::ActorGraphics3d>();
	//	actorGraphicsTwo->overLayColor = glm::vec3(0.0, 0.5, 0.0);

	//	std::printf("I'M THE HERO!");

	//});

	this->logic->getTimeManager()->getConflictSolver()->setOnReplayFinished([](nox::logic::world::Manager* manager){
		auto allActors = manager->getAllActors();
		for (int i = 3; i < allActors.size(); i++)
		{
			auto actorGraphics = allActors.at(i)->findComponent<nox::logic::graphics::ActorGraphics3d>();
			actorGraphics->overLayColor = glm::vec3(0.0, 0.5, 0.0);
		}
	});

	auto newActor1 = this->world->createActorFromDefinitionName("capsule");

	auto newBox1 = newActor1.get();
	auto actorTransform1 = newBox1->findComponent<nox::logic::actor::Transform3d>();
	actorTransform1->setPosition(glm::vec3(0.0, 0.0, 10));

	newActor1->create();
	newActor1->activate();

	int id = newActor1->getId().getValue();

	this->logic->getTimeManager()->getConflictSolver()->setConflictSolverFunction(newActor1->getId(), nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED, [](nox::logic::world::timemanipulation::ConflictParameters param){

			auto actorGraphicsOne = param.conflictingActor->findComponent<nox::logic::graphics::ActorGraphics3d>();
			actorGraphicsOne->overLayColor = glm::vec3(0.0, 0.5, 0.0);
		
			auto actorPhysics = param.conflictingActor->findComponent<nox::logic::physics::ActorPhysics3d>();
			//actorPhysics->setSimulatedFlag(false);

			std::printf("LOLTROLL");

			auto actorGraphicsTwo = param.affectedActor->findComponent<nox::logic::graphics::ActorGraphics3d>();
			actorGraphicsTwo->overLayColor = glm::vec3(0.0, 0.5, 0.0);
	});

	this->window->setControlledActor(newActor1.get());

	this->world->manageActor(std::move(newActor1));

	for (int i = 0; i < 25; i++)
	{
		auto newActor = this->world->createActorFromDefinitionName("box");

		auto newBox = newActor.get();

		int id3 = newActor->getId().getValue();

		auto actorTransform = newBox->findComponent<nox::logic::actor::Transform3d>();
		actorTransform->setPosition(glm::vec3(i % 5, (int)(i / 5), 0));

		newActor->create();
		newActor->activate();

		this->logic->getTimeManager()->getConflictSolver()->setConflictSolverFunction(newActor->getId(), nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED, [](nox::logic::world::timemanipulation::ConflictParameters param){
			auto actorGraphicsOne = param.conflictingActor->findComponent<nox::logic::graphics::ActorGraphics3d>();
			actorGraphicsOne->overLayColor = glm::vec3(0.0, 0.5, 0.0);

			auto actorGraphicsTwo = param.affectedActor->findComponent<nox::logic::graphics::ActorGraphics3d>();
			actorGraphicsTwo->overLayColor = glm::vec3(0.0, 0.5, 0.0);

		});


		this->world->manageActor(std::move(newActor));

	
		auto actorPhysics = newBox->findComponent<nox::logic::physics::ActorPhysics3d>();
		//auto actorLight = newBox->findComponent<nox::logic::graphics::ActorLight3d>();
		
		//Wall:
		actorPhysics->setPosition(glm::vec3(i % 5, (int)(i / 5), 0));
		

		float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		float g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		float b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

		//actorLight->getLight("pointLight2")->setColor(glm::vec3(r, g, b));

		//Plane
		//actorPhysics->setPosition(glm::vec3(i % 10, 0, (int)(i / 10)));
		//actorTransform->setPosition(glm::vec3(i % 50, 0, (int)(i / 50)));
		// Wall:
		actorPhysics->setPosition(glm::vec3(i % 5, (int)(i / 5) + i/10, 0));
		actorTransform->setPosition(glm::vec3(i % 5, (int)(i / 5) + i/10, 0));



		// Tower:
		//actorPhysics->setPosition(glm::vec3(0, i, 0));
		//actorTransform->setPosition(glm::vec3(0, i, 0));
	}

	//// PERFORMANCE TESTING /////////////////////////////////////////////////////////


	return true;
}

void TrdApplication::onDestroy()
{
	this->SdlApplication::onDestroy();

	this->listener.stopListening();
	this->logic = nullptr;

	this->logger.verbose().raw("Destroyed");
}

void TrdApplication::onUpdate(const nox::Duration& deltaTime)
{
	this->SdlApplication::onUpdate(deltaTime);

	//float tmp = this->getTps();
	//if (tmp < lowestFps)
	//	lowestFps = tmp;
	//this->logger.info().format("TPS: %.4f", lowestFps);
	

	const std::shared_ptr<nox::app::graphics::Camera3d> camera = this->window->getCamera();
	
	int x = 0;
	int y = 0;

	if (camera != nullptr)
	{
		if (buttons[UP])
		{
			camera->moveForward();
		}
		if (buttons[LEFT])
		{
			camera->moveLeft();
		}
		if (buttons[DOWN])
		{
			camera->moveBackward();
		}
		if (buttons[RIGHT])
		{
			camera->moveRight();
		}
		if (buttons[LSHIFT])
		{
			camera->setMoveSpeed(1.0f);
		}
		else
		{
			camera->setMoveSpeed(0.1f);

		}
		if (mouseOver(&x, &y))
		{
			camera->mouseUpdate(x, y);
		}
		
	}

	//repositionSkydome(camera->getPosition());

	if (this->window != nullptr)
	{
		this->window->render();
	}
}

void TrdApplication::onSdlEvent(const SDL_Event& event)
{
	this->SdlApplication::onSdlEvent(event);
	
	this->window->onSdlEvent(event);

	switch (event.type)
	{
	case(SDL_KEYDOWN) :
		switch (event.key.keysym.sym)
	{
		case(SDLK_w) :
			buttons[UP] = true;
			break;
		case(SDLK_a) :
			buttons[LEFT] = true;
			break;
		case(SDLK_s) :
			buttons[DOWN] = true;
			break;
		case(SDLK_d) :
			buttons[RIGHT] = true;
			break;
		case(SDLK_LSHIFT) :
			buttons[LSHIFT] = true;
			break;
		case(SDLK_SPACE) :
			this->window->getCamera()->reset();
			break;
		case(SDLK_x) :
		{
			//this->window->getIRenderer()->toggleDebugRendering();
			bool enabled = this->window->getIRenderer()->getDebugRenderer()->getIsDebugModeEnabled();
			auto debugRenderEnabledEvent = std::make_shared<nox::logic::graphics::DebugRenderingEnabled>(!enabled);
			this->logic->getEventBroadcaster()->queueEvent(debugRenderEnabledEvent);
		}
			break;
		case(SDLK_p) :
		{
			if (this->logic->getTimeManager() != nullptr)
				this->logic->getTimeManager()->setPause(!this->logic->getTimeManager()->isGamePaused());
			else
				this->logic->pause(!this->logic->isPaused());
			lowestFps = 100;
		}
			break;
		case(SDLK_BACKSPACE) :
			if (this->logic->getTimeManager() != nullptr)
			{
				this->logic->getTimeManager()->rewind();
			}
			break;
		case(SDLK_RETURN) :
			if (this->logic->getTimeManager() != nullptr)
			{
				this->logic->getTimeManager()->play();
			}
			break;
		case(SDLK_c) :
		{
			if (this->window->getControlledActor() != nullptr)
			{
				auto actor = this->window->getControlledActor();
				if (actor->findComponent<nox::logic::graphics::ActorGraphics3d>()->getAnimationIndex() == -1)
				{
					auto gr = actor->findComponent<nox::logic::graphics::ActorGraphics3d>();
					gr->setAnimation(0, true, gr->getAnimationSpeed());
				}
				else
				{
					auto gr = actor->findComponent<nox::logic::graphics::ActorGraphics3d>();
					gr->setAnimation(-1, true, gr->getAnimationSpeed());
				}
			}
		}
			break;
		default:
			break;
	}
		break;
	case(SDL_KEYUP) :
		switch (event.key.keysym.sym)
	{
		case(SDLK_w) :
			buttons[UP] = false;
			break;
		case(SDLK_a) :
			buttons[LEFT] = false;
			break;
		case(SDLK_s) :
			buttons[DOWN] = false;
			break;
		case(SDLK_d) :
			buttons[RIGHT] = false;
			break;
		case(SDLK_LSHIFT) :
			buttons[LSHIFT] = false;
			break;
		default:
			break;
	}
		break;
	case(SDL_MOUSEBUTTONDOWN) :
		switch (event.button.button)
	{
		case(SDL_BUTTON_MIDDLE) :
		{
			auto newActor = this->world->createActorFromDefinitionName("box");

			if (newActor != nullptr && this->logic->getTimeManager()->isRewindEnabled() == false && this->logic->getTimeManager()->isGamePaused() == false)
			{
				auto newBox = newActor.get();
				glm::vec3 camPos = this->window->getCamera()->getPosition();

				auto actorTransform = newBox->findComponent<nox::logic::actor::Transform3d>();
				actorTransform->setPosition(glm::vec3(camPos.x, camPos.y - 0.5f, camPos.z));

				newActor->create();
				newActor->activate(); 


				this->logic->getTimeManager()->getConflictSolver()->setConflictSolverFunction(newActor->getId(), nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED, [](nox::logic::world::timemanipulation::ConflictParameters param){
					auto actorGraphicsOne = param.conflictingActor->findComponent<nox::logic::graphics::ActorGraphics3d>();
					actorGraphicsOne->overLayColor = glm::vec3(0.0, 0.5, 0.0);

					auto actorGraphicsTwo = param.affectedActor->findComponent<nox::logic::graphics::ActorGraphics3d>();
					actorGraphicsTwo->overLayColor = glm::vec3(0.0, 0.5, 0.0);

				});

				
				this->world->manageActor(std::move(newActor));

				auto actorPhysics = newBox->findComponent<nox::logic::physics::ActorPhysics3d>();
				
				actorPhysics->setLinearVelocity(this->window->getCamera()->getViewDirection() * 50.0f);

				printf("actor created! Position %f %f %f\n", actorTransform->getPosition().x, actorTransform->getPosition().y, actorTransform->getPosition().z);

			}
		}
			break;
		default:
			break;
	}
	default:
		break;
	}	
}

bool TrdApplication::mouseOver(int*x, int*y)
{
	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_LEFT)/*true*/)
	{
		SDL_GetMouseState(x, y);
		return true;
	}
	return false;
}

void TrdApplication::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	if (event->isType(event::TPS_UPDATE))
	{	
		//this->logger.info().format("TPS: %.4f", this->getTps());
		//this->tpsUpdateTimer.reset();

		//this->logic->getEventBroadcaster()->queueEvent(event::TPS_UPDATE);
	}
	else if (event->isType(nox::logic::event::Manager::BROADCAST_COMPLETE_EVENT))
	{
	}
}


void TrdApplication::repositionSkydome(glm::vec3 cameraPosition)
{
	auto actor = this->world->findActor(nox::logic::actor::Identifier(2));

	auto transformComponent = actor->findComponent<nox::logic::actor::Transform3d>();

	//transformComponent->setPosition(glm::vec3(cameraPosition.x, 0, cameraPosition.z));
	glm::vec3 rotationalAxis(0, 1, 0);
	angle += ANGLE_INCREASE;
	glm::quat rotation(glm::angleAxis(angle , rotationalAxis));
	transformComponent->setRotation(rotation);
}